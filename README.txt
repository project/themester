// $Id:

README
----------------------------------------------------------------------
Themester module allows automated file association based on the url path
information.
This module was initially built to help with cascading CSS style sheets
properly based on the url taxonomy for any given page.
Admin screen is mainly to accommodate css organization for themester, however
Themester can be used to load any file type into the system via API functions.




INSTALLATION
----------------------------------------------------------------------
1. Copy the themester module directory to your modules directory
(Usually sites/all/modules/)
2. Enable the module from admin/build/modules
3. Setup the themester configurations at admin/settings/themester




ADMIN CONFIGURATIONS
----------------------------------------------------------------------
Themester Global Configurations
  Themester directory
    This is the directory that will be looked at as the root for style sheets
    This directory should be created within the current active theme directory.
    Auto creation of this directory was not built in to maintain consistency in
    security.
  Themester global style sheet
    Since Drupal inherently checks for style.css in our theme directory, organization
    can be harder to obtain if working with extremely large sites. This global style sheet
    will load in front of all other style sheets and can also be checked against for
    browser specifics.
  Themester browser assignments
    Browser assignments are specific browsers that we want to load different style sheets for.
    browser assignment library file are located in themester/lib. These can be added to very
    easily if there is an additional browser that we need to check for. Each browser specific file
    will need to be named with a reference to the browser that needs to load it.
    EXAMPLE: IE6 Specific Style Sheets - (filename-ie6.css)
  Directory nesting
    Directory nesting is allowing the url path information to be used as reference to directory
    organization.

Themester Views Configurations
  Themester Views integration
    This turns on views checking.
  Themester views directory
    This is a unique directory that should be used to organize our views specific style sheets that
    we want to load. (this can be located within our Themester directory.)
  Directory nesting
    Turns on directory nesting for views checking
  Views browser dependancy
    Turns on the browser checking for views files.




EXAMPLE USAGE
----------------------------------------------------------------------
For this example we will look at the url yoursite.com/admin/settings/themester
Once themester has been installed, it will automatically begin searching for files within
the current active theme directory.

If a directory is specified in the "Themester directory" field on the settings page, then themester
will search within that directory as the root for all style sheets.
NOTE: Themester does not overwrite the built in style sheet loading functionality, so if style.css
is located within your theme directory it will still be picked up. This is why we added the global
style sheet definition. This way global style sheets can be specified in order to maintain organizational
consistency.

Lets specify a directory of "css" in the themester directory field and a themester global style sheet
named "global". We do not include any extensions on this administration form.

NOTE: Drupal will automatically pull style.css from your theme's root if it exists.

With these settings themester will search for and load if found the following style sheets (from our
theme directory).
1. css/global.css
2. css/admin.css
3. css/settings.css
4. css/themester.css

if we specify specific browsers to load alternate style sheets in the themester browser assignment list,
then themester will check against the users browser and load alternate style sheets if found.
NOTE: Browser specific style sheets are loaded via javascript to insure compatibility with page caching. Hence
Drupals css aggregation will work for global style sheets, but not for browser specific style sheets.

If we were to specify that we want themetser to check for ie6 and load alternate style sheets for this
browser then themester would load any of the above style sheets that were found, and would also look 
for the following style sheets and include them with javascript validation against the useragent var:
1. css/global-ie6.css
2. css/admin-ie6.css
3. css/settings-ie6.css
4. css/themester-ie6.css

if we were to turn on directory nesting then themester would break out the url path information and
interpret them not only as files to search for, but also possible directories. This allows us to organize
different sections of the site as a hirearchy of directories to insure we don't fall into a cascade that
cannot be overwritten.

With directory nesting turned on and with our current settings... themester will search for and load
the following (if found):
1. css/global.css
2. css/admin.css
3. css/settings.css
4. css/themester.css
5. css/admin/global.css
6. css/admin/admin.css
7. css/admin/settings.css
8. css/admin/themester.css
9. css/admin/settings/global.css
10. css/admin/settings/admin.css
11. css/admin/settings/settings.css
12. css/admin/settings/themester.css
13. css/admin/settings/themester/global.css
14. css/admin/settings/themester/admin.css
15. css/admin/settings/themester/settings.css
16. css/admin/settings/themester/themester.css

Then themester will also check for any browser specifics within the same directories.
NOTE: With directory nesting turned on, we can see how it could become very taxing on the system
if we do not have css aggregation and page caching turned on. So keep this in mind.
Also... Themester searches for all the files in one batch, then prints them in order. This helps
optimization a bit, but these directories can easily get out of hand...lol.




VIEWS INTEGRATION
----------------------------------------------------------------------
Views style sheet loading work the same as page style sheets with one difference.
Views style sheets load based on the view name, and differ between page and block versions
of the view in mention.
Example filenames:
  my_view_page.css
  my_view_page-ie6.css
  my_view_block.css
  my_view_block-ie6.css




API USAGE
----------------------------------------------------------------------
Themester can be utilized to load other file type that may be needed with this same url taxonomy
logic.

themester_load_file_list() is the main function that does all the work
This function accepts the following parameters.
$file_name = This can be either a string containing a file name or an array containing multiple file names to load.
             if this parameter is specified, then themester will only look for what it has been given. If this parameter
             is NULL, then themester will search for the url information as the files that need to be looked for.
$search_directory = String -If specified this is the directory that themester should use as the root to search through.
$search_directories = Boolean - this determines if directory nesting should be initialized.
$file_extension = String - provides the file extension to search for.
$check_browser_args = Boolean - Check for browser specifics. If no browser assignments are selected in the settings, then this will default to FALSE.

themester_load_file_list($file_name = NULL, $search_directory = NULL, $search_directories = NULL, $file_extension = '.css', $check_browser_args = TRUE)


When checking for other file types, themester will automatically check if there is a theme function with that name.
for example: 
themester_load_file_list(NULL, path_to_theme() . '/xml', FALSE, '.xml', FALSE);
this will try and locate the filenames based on the url path with the extension .xml within the current theme location /xml.

Themester runs all final output through theme functions. For CSS this function is called theme_themester_add_css($file_list);
Themester will look for different theme functions based on the extension that it is searching for.
So in the above example themester will only load the xml files that it find if it is told to do so within a theme function defined
as theme_themester_add_xml($file_list);
$file_list is the array of files that were found.

NOTE: Themester only has themester_add_css defined, so any other file extensions should have the provided theme function in template.php
if we want the files to actually load. If the theme function is not found, the files will not load.
