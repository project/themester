// $Id:

README
----------------------------------------------------------------------
Themester browser assignments are built by adding additional browser specific .lib file to the
themester/lib directory.

Themester uses the user agent variable in javascript to test browsers against.

Its a simple syntax as follows (Example for Internet Explorer 6):

label::IE6 Specific Style Sheets
extension::ie6
test_string::MSIE 6.0

label::will define the string that appears to the left of the filename example in the select element.
extension::will define the appended browser string to the filenames.
test_string::will determine what themester checks against in the user agent variable.

you can just duplicate an included .lib file, rename it and then make the needed browser changes.