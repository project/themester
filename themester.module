<?php

/**
 * @file
 * Provides functionality for url based style sheet association.
 *
 * Themester allows themers and front-end developers to cascade dynamically
 * through css style sheets based on the url path (query string) information.
 *
 * The Themester administration settings allow developers to define the following:
 *   The themester search directory for page based style sheets.
 *   A global style sheet name
 *   Browser specific style sheet definitions
 *   Directory searching files
 *   Views integration
 *     Views search directory for view based style sheets.
 *     Directory searching for views files
 *     Browser specific style sheet loading for views.
 *
 * Themester has also been updated to work with any file types.
 */

/**
 * Implementation of hook_help();
 */
function themester_help($section) {
  switch ($section) {
    case 'admin/help#themester':
      $output = themester_get_text('help_text');
      return $output;
  }
}// end function themester_help();

/**
 * Implementation of hook_perm();
 */
function themester_perm() {
  return array('configure themester');
}// end function themester_perm();

/**
 * Implementation of hook_menu();
 */
function themester_menu($may_cache) {
  global $user;
  $items = array();
  
  if ($may_cache) {
    $items[] = array(
      'path' => 'admin/settings/themester',
      'title' => t('Themester'),
      'description' => t('Themester configuration settings.'),
      'callback' => 'drupal_get_form',
      'callback arguments' => array('themester_admin_settings'),
      'access' => user_access('configure themester'),
    );
    
    return $items;
  }
  if (!$may_cache) {
    themester_load_file_list();
  }
}// end function themester_menu();


/*****************************************
 ************ MENU CALLBACKS *************
 *****************************************/
/**
 * Callback from hook_menu()
 
 * Provides form elements for user specified directory.
 * Provides form elements for declaring browser specific style sheet extensions.
 */
function themester_admin_settings() {
  // Global settings form
  $form['themester_settings'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Themester Global Configurations'),
    '#collapsible' => TRUE,
    '#collapsed'   => FALSE,
  );
  
  $form['themester_settings']['themester_directory'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Themester directory'),
    '#default_value' => variable_get('themester_directory', t('')),
    '#description'   => themester_get_text('form_themester_directory'),
    '#size'          => 60,
  );
  
  $form['themester_settings']['themester_global_css'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Themester global stylesheet'),
    '#default_value' => variable_get('themester_global_css', t('')),
    '#description'   => themester_get_text('form_themester_global_css'),
    '#size'          => 60,
  );
  
  $browser_args = themester_get_browser_lib();
  if (count($browser_args) > 0) {
    $options = array();
    foreach ($browser_args as $browser) {
      $options[$browser['test_string'] . '::' . $browser['extension']] = $browser['label'] . ' - (filename-' . $browser['extension'] . '.css)';
    }//end - foreach
    
    $form['themester_settings']['themester_browser_args'] = array(
      '#type'          => 'select',
      '#title'         => t('Themester browser assignments.'),
      '#default_value' => variable_get('themester_browser_args', ''),
      '#multiple'      => TRUE,
      '#options'       => $options,
      '#description'   => themester_get_text('form_themester_browser_args'),
    );
  }
  
  $form['themester_settings']['themester_directory_search'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Directory nesting.'),
    '#default_value' => variable_get('themester_directory_search', 0),
    '#description'   => themester_get_text('form_themester_directory_search'),
  );
  
  // Views Integration settings form
  $form['themester_views_settings'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Themester Views Configurations'),
    '#collapsible' => TRUE,
    '#collapsed'   => TRUE,
    '#description' => themester_get_text('form_themester_views_desc'),
  );
  
  $available = (module_exists('views'))? FALSE : TRUE;
  
  $form['themester_views_settings']['themester_views_enabled'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Themester Views integration.'),
    '#default_value' => variable_get('themester_views_enabled', 0),
    '#description'   => themester_get_text('form_themester_views_enabled'),
    '#disabled'      => $available,
  );
  
  $form ['themester_views_settings']['themester_views_directory'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Themester Views directory'),
    '#default_value' => variable_get('themester_views_directory', t('')),
    '#description'   => themester_get_text('form_themester_directory'),
    '#size'          => 60,
    '#disabled'      => $available,
  );
  
  $form['themester_views_settings']['themester_views_directory_search'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Directory nesting.'),
    '#default_value' => variable_get('themester_views_directory_search', 0),
    '#description'   => themester_get_text('form_themester_directory_search'),
    '#disabled'      => $available,
  );
  $form['themester_views_settings']['themester_views_browser_args'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Views browser dependancy.'),
    '#default_value' => variable_get('themester_views_browser_args', 0),
    '#description'   => themester_get_text('form_themester_views_browser_args'),
    '#disabled'      => $available,
  );

  return system_settings_form($form);
}// end function themester_admin_settings();

/**
 * Validate the administration form
 */
function themester_admin_settings_validate($form_id, $form_values) {
  if ($form_id === 'themester_admin_settings') {
    $exp = "/^\//";
    if (preg_match($exp, $form_values['themester_directory'])) {
      form_set_error($form_id, t('Enter a directory path without beginning the beginning "/"'));
    }
    
    $exp = "/^[a-zA-Z][\w]+([^\.](^.+)?)?$/";
    if (!preg_match($exp, $form_values['themester_global_css'])) {
      form_set_error($form_id, t('Enter a valid file name beginning with an alpha character and without the trailing extension.'));
    }
    
    $exp = "/^\//";
    if (preg_match($exp, $form_values['themester_views_directory'])) {
      form_set_error($form_id, t('Enter a directory path without beginning the beginning "/"'));
    }
  }
}//end - function


/*****************************************
 ********** INTERNAL FUNCTIONS ***********
 *****************************************/
/**
 * Return an array of files to load.
 * This function will return both the browser argument list
 * as well as the global list of files that were found and should be loaded.
 *
 * @param $search_directory
 *   String containing the directory to search in.
 * @param $check_browser_args
 *   Boolean to determine in the function should check for a browser specific list of files.
 *   If neither TRUE is specified then the function will base its browser return on the global themester
 *   directory search setting.
 * @param $file_extension
 *   String indicating the file extension to look for. This allows for multiple file types to leverage this function.
 * @param $file_name
 *   String indicating a specific filename to search for. This allows for specific files to be loaded instead of the default
 *   functionality which is to look for the url string section.
 *
 * return
 *   Array containing the files that were found and should be loaded.
 */
function themester_load_file_list($file_name = NULL, $search_directory = NULL, $search_directories = NULL, $file_extension = '.css', $check_browser_args = TRUE) {
  // prepare the file extension
  $file_extension = trim($file_extension);
  
  //define the inner directory searching setting
  $search_directories = (is_bool($search_directories))? $search_directories : themester_get_directory_search_setting();
  
  // instantiate return array
  $return_arr      = array();
  $return_arr['global'] = array();
  $return_arr['browser'] = array();
  $return_arr['type'] = trim($file_extension, '.');
  
  // insure the check_browser_args var
  $browser_check = ($check_browser_args === TRUE)? TRUE : FALSE;
  
  // get browser information
  if ($browser_check) {
    $browser_vars = variable_get('themester_browser_args', NULL);
    $browser_check = (count($browser_vars))? TRUE : FALSE;
    $browser_args = ($browser_check)? themester_break_arguments($browser_vars) : '';
  }
  
  // insure that we are working with an array
  if (is_null($file_name) || empty($file_name)) {
    $file_list = themester_get_url_array();
    $global_file = variable_get('themester_global_css', '');
    if (!empty($global_file)) {
      array_unshift($file_list, $global_file);
    }
  } else if (is_array($file_name)) {
    $file_list = $file_name;
  } else {
    $file_list = (empty($file_name))? array() : array($file_name);
  }
  
  $filename_list = themester_generate_filename_search_list($file_list, $file_extension, $browser_args);
  
  // get the directories to search through
  $directory_search_list = themester_generate_directory_list($search_directory, $search_directories);
  $directory_load_list = themester_generate_directory_list($search_directory, $search_directories, TRUE);
  
  // populate the return file list
  foreach ($directory_search_list as $dir => $val) {
    foreach ($filename_list['global'] as $gfile => $gname) {
      $search_file = $val . $gname;
      if (themester_locate_file($search_file)) {
        $load_file = $directory_load_list[$dir] . $gname;
        $return_arr['global'][] = $load_file;
      }
    }//end - foreach
    if (isset($filename_list['browser'])) {
      foreach ($filename_list['browser'] as $br =>$bdata) {
        $browser_arr = array();
        foreach ($bdata as $bfile => $bval) {
          $name = $bval['filename'];
          $cond = $bval['condition'];
          
          $search_file = $val . $name;
          if (themester_locate_file($search_file)) {
            $load_file = $directory_load_list[$dir] . $name;
            $browser_arr[] = $load_file;
            $return_arr['browser'][$br]['filelist'][] = $load_file;
            if (!isset($return_arr['browser'][$br]['condition'])) {
              $return_arr['browser'][$br]['condition'] = $cond;
            }
          }
        }//end - foreach
      }//end - foreach
    }
  }//end - foreach
  
  theme('themester_load_files', $return_arr);
}//end - function

/**
 * Generate the directory list to search through
 *
 * @param $search_dir
 *   String
 *
 * return
 *   Array containing all the directories to search through
 */
function themester_generate_directory_list($search_dir = NULL, $dir_search = NULL, $return_relative = FALSE) {
  $return_arr = array();
  
  if (!empty($search_dir) && $search_dir !== NULL) {
    $root_search_path = ($return_relative)? $search_dir : themester_get_base_search_path() . $search_dir;
  } else {
    $search_dir = path_to_theme();
    $base_path        = ($return_relative)? $search_dir : themester_get_base_search_path() . $search_dir;
    $themester_dir    = themester_get_global_dir();
    $root_search_path = (empty($themester_dir))? $base_path : $base_path . '/' . $themester_dir;
  }
  
  $root_search_path  .= '/';
  $return_arr[]       = $root_search_path;
  
  $dir_search = (is_null($dir_search))? FALSE : $dir_search;
  
  if ($dir_search) {
    $url_list           = themester_get_url_array();
    foreach ($url_list as $idx => $path) {
      $root_search_path .= $path . '/';
      $return_arr[] = $root_search_path;
    }//end - foreach
  }
  
  return $return_arr;
}//end - function

/**
 * Generate the main file list to search for.
 * 
 * @param $file_list
 *   Array containing the files that need to be search for.
 * @param $ext
 *   String containing the files extension
 *
 * return
 *   Array containing the file data
 */
function themester_generate_filename_search_list($file_list = array(), $ext = '.css', $browser_args = '') {
  $ret_arr      = array();
  
  if (is_array($file_list)) {
    foreach ($file_list as $idx => $path) {
      $ret_arr['global'][] = themester_generate_filename($path, $ext);
    }//end - foreach
    
    if (!empty($browser_args)) {
      foreach ($browser_args as $bx => $arg) {
        $count = 0;
        foreach ($file_list as $dx => $file) {
          $ret_arr['browser'][$arg[1]][$count]['filename'] = themester_generate_filename($file, $ext, $arg[1]);
          $ret_arr['browser'][$arg[1]][$count++]['condition'] = $arg[0];
          $count++;
        }//end - foreach
      }//end - foreach
    }
  }
  
  return $ret_arr;
}//end - function

/**
 * Generate the browser specific file list
 *
 * @param $file_list
 *   Array containing the files to be updated
 * @param $browser_args
 *   Array containing the browser argument data
 * @param $ext
 *   String containing the files extension to be used.
 * 
 * return
 *   Array with the structured files returned.
 */
function themester_generate_browser_files($file_list, $browser_args, $ext = '.css') {
  $ret_arr = array();
  
  if (count($browser_args) > 0 && count($file_list) > 0) {
    foreach ($browser_args as $br => $arg) {
      $condition   = $arg[0];
      $browser_ext = $arg[1];
      $browser_file_arr = array();
      foreach ($file_list as $idx => $file) {
        $browser_file_arr[] = array(
          'name' => themester_generate_filename($file, $ext, $browser_ext),
          'condition' => $condition,
          'ext'       => $browser_ext,
        );
      }//end - foreach
      
      $ret_arr[] = $browser_file_arr;
      
    }//end - foreach
  }
  
  return $ret_arr;
}//end - function

/**
 * Generate the filename to search for
 *
 * @param $filename
 *   String containing the files name w/o the extension
 * @param $ext
 *   String containing the file extension to append to the filename
 * @param $extra
 *   String containing any text to append to the filename before the extension is applied.
 *   extra text will be applied after a dash (-)
 *
 * return
 *   String containing the final filename.
 */
function themester_generate_filename($filename, $ext = '.css', $extra = '') {
  $return = $filename;
  if (!empty($extra)) {
    $return .= "-{$extra}";
  }
  $return .= $ext;
  
  return $return;
}//end - function

/**
 * break apart the argument list into usable data
 *
 * @param $arr
 *   Array containing the stored browser arguments
 * return
 *   Array containing the separated data
 */
function themester_break_arguments($arr) {
  $ret_arr = array();
  
  foreach ($arr as $idx => $arg) {
    $temp = explode('::', $arg);
    $ret_arr[] = $temp;
  }//end - foreach
  
  return $ret_arr;
}//end - function

/**
 * Get the base path for the site
 * Used for directory searching
 * This function will return the full base path
 *
 * return $return
 *   String containing the base search path
 */
function themester_get_base_search_path() {
  return $_SERVER['DOCUMENT_ROOT'] . base_path();
}//end - function

/**
 * get the themester global directory
 *
 * return
 *   String
 */
function themester_get_global_dir() {
  $return = variable_get('themester_directory', '');
  return $return;
}//end - function

/**
 * get the admin settings for directory searching
 *
 * return
 *   Boolean
 */
function themester_get_directory_search_setting() {
  $setting = variable_get('themester_directory_search', 0);
  $return = ($setting)? TRUE : FALSE;
  
  return $return;
}//end - function

/**
 * Get the views search directory
 *
 * return
 *   String
 */
function themester_get_global_view_dir() {
  $return = variable_get('themester_views_directory', '');
  return $return;
}//end - function

/**
 * Get the current url string parsed into an array for searching.
 *
 * return
 *   Array
 */
function themester_get_url_array() {
  // get initial path values
  $path_alias = drupal_get_path_alias($_GET['q']);
  $url_arr = split('/', $path_alias);
  
  return $url_arr;
}//end - function

/**
 * Get the initial js to load browser specifics into page.
 *
 * return $output;
 *   String containing the initial js for checking the browser.
 */
function themester_get_js_agent() {
  return theme('themester_js_agent');
}//end - function

/**
 * Return the contents of the themester lib directory
 * as an array. 
 * This function can probably be optimized a bit more.
 *
 * return
 *   Array containing the content files values.
 */
function themester_get_browser_lib() {
  $browser_file_data = array();
  $lib_path = drupal_get_path('module', 'themester') . '/lib';
  $dir = opendir($lib_path);
  while (($file = readdir($dir)) !== FALSE) {
    if (themester_verify_lib_files($file)) {
      $file_str = file_get_contents($lib_path . '/' . $file);
      $file_lines = explode("\n", $file_str);
      $file_arr = array();
      foreach ($file_lines as $idx => $line) {
        $data = explode("::", $line);
        $label = $data[0];
        $content = $data[1];
        $file_arr[$label] = $content;
      }//end - foreach
      
      $browser_file_data[] = $file_arr;
    }
  }//end - while
  closedir($dir);
  
  return $browser_file_data;
}//end - function

/**
 * Validate the lib file list
 * @param $file
 *   String containing the filename
 *
 * return
 *  Boolean
 */
function themester_verify_lib_files($file) {
  $return = FALSE;
  $exp = "/^[a-zA-Z]+([\w]+)?[\.]{1}[l][i][b]$/";
  if (preg_match($exp, $file)) {
    $return = true;
  }
  
  return $return;
}//end - function

/**
 * Locate the requested file
 *
 * @param $file
 *   Full path to search for
 *
 * return
 *   Boolean
 */
function themester_locate_file($file) {
  $return = FALSE;
  
  if (file_exists($file)) {
    $return = TRUE;
  }
  
  return $return;
}//end - function

/**
 * Implementation of hook_views_post_view
 * Allows views integration for cascading view based style sheets.
 */
function themester_views_post_view(&$view) {
  $views_enabled = variable_get('themester_views_enabled', 0);
  
  if ($views_enabled) {
  $views_directory            = variable_get('themester_views_directory', '');
  $views_directory_searching  = variable_get('themester_views_directory_search', 0);
  $browser_specific_searching = variable_get('themester_views_browser_args', 0);
  
  $directory         = (empty($views_directory))? path_to_theme() : path_to_theme() . '/' . $views_directory;
  $dir_searching     = ($views_directory_searching)? TRUE : FALSE;
  $browser_searching = ($browser_specific_searching)? TRUE : FALSE;
  
    switch ($view->build_type) {
      case 'page':
        $filename = $view->name . '_page';
        themester_load_file_list($filename, $directory, $dir_searching, '.css', $browser_searching);
      break;
      case 'block':
        $filename = $view->name . '_block';
        themester_load_file_list($filename, $directory, $dir_searching, '.css', $browser_searching);
      break;
      default:
      break;
    }//end - switch
  }
}//end - function
 
 
/*****************************************
 ************* THEME HOOKS ***************
 *****************************************/
 /**
  * Theme the filelist output for loading
  *
  * @param $file_list
  *   Array containing the full list of files to load
  */
function theme_themester_load_files($file_list) {
  if (is_array($file_list)) {
    $type = $file_list['type'];
    $global_list = $file_list['global'];
    $browser_list = $file_list['browser'];
    
    if ($type === 'css') {
      theme('themester_add_css', $global_list, $browser_list);
    } else {
      $function = 'themester_add_' . $type;
      $function_located = theme_get_function($function);
      if ($function_located) {
        theme($function, $global_list, $browser_list);
      }
    }
  }
}//end - function
 
/**
 * Load all the style sheets found using drupal_add_css();
 *
 * @pram $file_list
 *   Array containing the files that were found to exist in the order they should load.
 *   ex: $file_list = array(
 *         0 => array(
 *           'filedata' => 'path_to_theme() . 'filename.css',
 *           'location' => 'theme',
 *         );
 *       );
 */
function theme_themester_add_css($global_list = NULL, $browser_list = NULL) {
  if (is_array($global_list) && count($global_list)) {
    foreach ($global_list as $idx => $file_data) {
      // Insure that the information that we are attempting to load
      // is indeed a valid string... if so, then run the string through t()
      $filename = (is_string($file_data))? t($file_data) : '';
      if (!empty($filename)) {
        drupal_add_css($filename, 'theme');
      }
    }//end - foreach
  }
  
  if ($browser_list !== NULL && count($browser_list)) {
    themester_get_js_agent();
    $js = '';
    foreach ($browser_list as $bid => $data) {
      $ext   = $bid;
      $files = $data['filelist'];
      $cond  = $data['condition'];
      
      $js   .= theme('themester_browser_conditional', $files, $cond, $ext);
    }//end - foreach
    // only write the javascript if we actually have files to load.
    if (!empty($js)) {
      drupal_add_js($js, 'inline');
    }
  }
}//end - function

/**
 * Return the js agent variable
 * Intentionally themeable
 * This probably shouldn't need to be messed with, but its always good
 * incase a themer needs to change the way that the js is loading the style sheets.
 *
 * return $output
 *   String that contains the initial js variable that is being checked against
 *   for js based style loading.
 */
function theme_themester_js_agent() {
  if (!defined('THEMESTER_JS_VAR')) {
    $output = "v_themester_agent_var = navigator.userAgent; \n";
    drupal_add_js($output, 'inline');
    define('THEMESTER_JS_VAR', 'v_themester_agent_var');
  }
}//end - function

/**
 * Theme the js conditionals
 */
function theme_themester_browser_conditional($file_list, $conditional, $ext) {
  $output  = "v_themester_{$ext} = '{$conditional}'; \n";
  $output .= "v_themester_match_{$ext} = " . THEMESTER_JS_VAR . ".search(v_themester_{$ext}); \n";
  $output .= "if (v_themester_match_{$ext} != -1) { \n";
  foreach ($file_list as $idx => $file) {
    $output .= "document.write(\"<style type='text/css' media='all'>@import '/" . $file . "';</style>\"); \n";
  }//end - foreach
  $output .= "} \n";
  
  return $output;
}//end - function


/*****************************************
 ************* TEXT OUTPUT ***************
 *****************************************/
/**
 * Provide the text output for all the large
 * bodies of text.
 *
 * @param $text
 *   This is a string corresponding to the appropriate text output request.
 *
 * return
 *   String that contains the requested text.
 *
 */
function themester_get_text($text) {
  $return = '';
  
  switch ($text) {
    case 'form_themester_directory':
      $return = t('Specify one directory, within the default theme, which Themester will automatically check for style sheets. ') . '<em>' . t('This entry is relative to your theme directory - /sites/all/themes/my_theme - and will access your theme root if no value is set.') . '</em><br />';
      $return .= '<strong>' . t('NOTE: If you specify a directory here you will need to create this directory within your theme directory.') . '<br />';
      $return .= t('This is the directory which all your styles should live in.') . '</strong>';
    break;
    case 'form_themester_global_css':
      $return = t('Drupal will automatically load the file style.css within your theme root.<br />');
      $return .= t('<strong>Alternately</strong>, If you want to store your global style sheet within the Themester directory for organizational consistency, you can specify a global style sheet name here. Themester will automatically load this style sheet first.<br />');
      $return .= t('The name of this style sheet should NOT be style.css, otherwise Drupal will recognize the themester css directory as a theme and it will load on your theme settings page as a different theme.');
    break;
    case 'form_themester_browser_args':
       $browser = $_SERVER['HTTP_USER_AGENT'];
  
      $return  = t('Browser specific style sheet assignments are included as individual files via javascript in order to work with page caching.<br />');
      $return .= t('If you want to check against a browser that is not in the list, you can add a .lib file to the themester/lib/ directory with the appropriate information for that browser.<br />');
      $return .= t('<strong>Instruction for adding browser selections can be found in themester/lib/README.txt</strong><br />');
    break;
    case 'form_themester_directory_search':
      $return = t('Enabling directory nesting tells Themester to reference the url path information as both directories and filenames.<br />');
      $return .= t('Themester will load all styles to cascade correctly.');
    break;
    case 'help_text':
      $return .= t('<p>The Themester module allows dynamic attachment of css style sheets based on url path information.');
      $return .= t(' You can define both the directory which Themester will search for style sheets as well as assign browser specific style sheet association and a alternate global style sheet file name from the ');
      $return .= l('Themester Configuration', 'admin/settings/themester') . t(' page.<br />');
      $return .= t('Themester searches for style sheets in a cascading fashion based on the url path (query string) information.<br />');
      $return .= t('<strong>EXAMPLE URL:</strong> <em>http://yoursite.com/section/subsection</em><br />');
      $return .= t('Themester will look for the following style sheets in the following order and will include each one it finds.') . '</p>';
      $return .= t('<ol>');
      $return .= t('<li>style.css<em> - located in your default theme directory.</em></li>');
      $return .= t('<li>section.css<em> - located in the user generated directory.</em></li>');
      $return .= t('<li>subsection.css<em> - located in the user generated directory.</em></li>');
      $return .= t('</ol>');
      $return .= t('<p><strong>NOTE:</strong> Themester does NOT search for appended string urls like the template system in the phptemplate engine. ');
      $return .= t('It was designed to cascade the styles from the top down and not to specifically overwrite entire style sheets. ');
      $return .= t('This idea was based on maintaining best practices for cascading style sheets from one section to the next in a hierarchy fashion.') . '</p>';
      $return .= t('<h3>INSTALLATION</h3>');
      $return .= t('<ol><li>Copy the themester module directory to your modules directory (Usually sites/all/modules/)</li><li>Enable the module from admin/build/modules</li><li>Setup the themester configurations at admin/settings/themester</li></ol>');
      $return .= t('<h3>ADMIN CONFIGURATIONS</h3>');
      $return .= t('Themester Global Configurations');
      $return .= t('<ul>');
      $return .= t('<li>Themester directory<br />This is the directory that will be looked at as the root for style sheets<br />This directory should be created within the current active theme directory.<br />Auto creation of this directory was not built in to maintain consistency in security.</li>');
      $return .= t('<li>Themester global style sheet<br />Since Drupal inherently checks for style.css in our theme directory, organization can be harder to obtain if working with extremely large sites. This global style sheet will load in front of all other style sheets and can also be checked against for browser specifics.</li>');
      $return .= t('<li>Themester browser assignments<br />Browser assignments are specific browsers that we want to load different style sheets for.<br />Browser assignment library file are located in themester/lib. These can be added to very easily if there is an additional browser that we need to check for. Each browser specific file will need to be named with a reference to the browser that needs to load it.<br /><strong>EXAMPLE:</strong> IE6 Specific Style Sheets - (filename-ie6.css)</li>');
      $return .= t('<li>Directory nesting<br />Directory nesting is allowing the url path information to be used as reference to directory organization.</li>');
      $return .= t('<li>Themester Views integration<br />This turns on views checking.</li>');
      $return .= t('<li>Themester views directory<br />This is a unique directory that should be used to organize our views specific style sheets that we want to load. (this can be located within our Themester directory.)</li>');
      $return .= t('<li>Directory nesting<br />Turns on directory nesting for views checking</li>');
      $return .= t('<li>Views browser dependancy<br />Turns on the browser checking for views files.');
    break;
    case 'form_themester_views_enabled':
      $return  = t('Enables views specific loading of style sheets.');
    break;
    case 'form_themester_views_browser_args':
      $return  = t('Allows views to access the browser assignments that have been selected. If there will be browser specific views style sheets, then this setting should be checked.');
    break;
    case 'form_themester_views_desc':
      $return  = t('Themester can auto load views based style sheets.<br />');
      $return .= t('Page and block view style sheets are searched for based on the view_name_block.css or view_name_page.css. This allows both block based views and page views to be styled differently.<br />');
      $return .= t('<strong>Views must be installed in order to use these features.</strong>');
    break;
    default:
    break;
  }//end - switch
  
  return $return;
}//end - function